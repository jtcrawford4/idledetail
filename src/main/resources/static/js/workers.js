function incrementWorkers(amount){
    WORKERS += amount;
    setWorkers($('#workers'), WORKERS);
    WORKERS == 0 ? $('#fireWorkerBtn').attr('disabled', true).removeClass('btn-danger') : $('#fireWorkerBtn').attr('disabled', false).addClass('btn-danger');
}

function incrementCutAndBuffWorkers(amount){
    WORKERS_CAB += amount;
    setWorkers($('#workersCutAndBuff'), WORKERS_CAB);
    WORKERS_CAB == 0 ? $('#fireWorkerCutAndBuffBtn').attr('disabled', true).removeClass('btn-danger') : $('#fireWorkerCutAndBuffBtn').attr('disabled', false).addClass('btn-danger');
}

function setWorkers($workersLbl, workers){
    $workersLbl.text(workers);
}

function setHireWorkerCost(amount){
    $('#hireWorkerBtn').val(amount);
    WORKER_UPGRADE_PRICE = amount;
}

function setHireCutAndBuffWorkerCost(amount){
    $('#hireWorkerCutAndBuffBtn').val(amount);
    WORKER_UPGRADE_PRICE_CAB = amount;
}

function workersUpgradable(){
    return (CASH >= parseInt($('#hireWorkerBtn').val()))
}

function cutAndBuffWorkersUpgradable(){
    return (CASH >= parseInt($('#hireWorkerCutAndBuffBtn').val()))
}

//todo refactor payroll functions
//todo fire more expensive workers first?
function payrollDue(){
    var $payrollBtn = $('#payrollBtn');
    var overdueAmount = parseInt($payrollBtn.val());
    var amountDue = ((WORKER_PAY * WORKERS) + (WORKERS_CAB * WORKER_PAY_CAB) + overdueAmount);
    if ((TEMP_WORKER_ELAPSED_TIME >= WORKER_PAYROLL_DUE) || (TEMP_WORKER_ELAPSED_TIME_CAB >= WORKER_PAYROLL_DUE)){
        if(ACCOUNTANT_HIRED && (CASH >= amountDue)){
            $payrollBtn.val(amountDue).click();
            $('#accountantPayrollLbl').text("Payroll paid - accountant");
            updateNotifications($('#accountantPayrollLbl'));
            TEMP_WORKER_ELAPSED_TIME = 0;
            TEMP_WORKER_ELAPSED_TIME_CAB = 0;
            return;
        }else{
             $('#accountantPayrollLbl').hide();
         }
        $payrollBtn.val(amountDue).text("Payroll Due: $" + amountDue);
        payrollPayable();
        updateNotifications($payrollBtn);
        TEMP_WORKER_ELAPSED_TIME = 0;
        TEMP_WORKER_ELAPSED_TIME_CAB = 0;
        MISSED_PAYROLLS++;
        if(MISSED_PAYROLLS > MISSED_PAYROLLS_THRESHOLD){
            missedPayroll();
        }
        else{
            $('#accountantPayrollLbl').hide();
            notificationsCheck();
        }
    }
}

function missedPayroll(){
    var $payrollBtn = $('#payrollBtn');
    var missedPayrolls = MISSED_PAYROLLS - 1; // - 1 because missed payrolls is automatically incremented when payroll is due
    unlockHireWorkersButton(false);
    if(WORKERS > 0){
        var $workers = $('#workers');
        $workers.css("color","#dc3545");
        blink($workers);
        WORKERS -= 1;
        setWorkers($workers, WORKERS);
        var currentAmountDue = $payrollBtn.val();
        var revisedAmountDue = currentAmountDue - WORKER_PAY;
        $payrollBtn.val(revisedAmountDue).text("Payroll Due: $" + revisedAmountDue);
        WORKER_UPGRADE_PRICE /= 1.25
    }
    if(WORKERS == 0){ //todo why doesnt this work in an else statement?
         $payrollBtn.click(); //hide div, pay what you owe in backpay. no workers left
         $payrollBtn.hide();
         notificationsCheck();
         WORKER_UPGRADE_PRICE = 200; //todo make constant value for original?
         $('#fireWorkerBtn').attr('disabled', true).removeClass('btn-danger');
    }
    if(WORKERS_CAB > 0){
        var $workers = $('#workersCutAndBuff');
        $workers.css("color","#dc3545");
        blink($workers);
        WORKERS_CAB -= 1;
        setWorkers($workers, WORKERS_CAB);
        var currentAmountDue = $payrollBtn.val();
        var revisedAmountDue = currentAmountDue - WORKER_PAY_CAB;
        $payrollBtn.val(revisedAmountDue).text("Payroll Due: $" + revisedAmountDue);
        WORKER_UPGRADE_PRICE_CAB /= 1.25
    }
}

$('#hireWorkerBtn').on('click', function(){
    incrementWorkers(1);
    incrementCash(-(WORKER_UPGRADE_PRICE))
    setHireWorkerCost(WORKER_UPGRADE_PRICE * 1.25);
});

$('#fireWorkerBtn').on('click', function(){
    incrementWorkers(-1);
    if(WORKERS == 0){
        $(this).attr('disabled', true).removeClass('btn-danger');
    }
    setHireWorkerCost(WORKER_UPGRADE_PRICE / 1.25);
});

$('#hireWorkerCutAndBuffBtn').on('click', function(){
    incrementCutAndBuffWorkers(1);
    incrementCash(-(WORKER_UPGRADE_PRICE_CAB))
    setHireCutAndBuffWorkerCost(WORKER_UPGRADE_PRICE_CAB * 1.25);
});

$('#fireWorkerCutAndBuffBtn').on('click', function(){
    incrementCutAndBuffWorkers(-1);
    if(WORKERS == 0){
        $(this).attr('disabled', true).removeClass('btn-danger');
    }
    setHireCutAndBuffWorkerCost(WORKER_UPGRADE_PRICE_CAB / 1.25);
});