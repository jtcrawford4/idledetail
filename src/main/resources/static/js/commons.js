function initialize(){
    setCash(CASH);
    setWorkers($('#workers'), WORKERS);
    setWorkers($('#workersCutAndBuff'), WORKERS_CAB);
    setLotCapacity(1);
    setLotCapacityCutAndBuff(1);
    setHireWorkerCost(WORKER_UPGRADE_PRICE);
    setHireCutAndBuffWorkerCost(WORKER_UPGRADE_PRICE);
    setLotCapacityCost(LOT_UPGRADE_PRICE);
    setCutAndBuffLotCapacityCost(LOT_UPGRADE_PRICE_CAB);
}

function blink($element){
    $element.fadeOut(500).fadeIn(500);
}

function toCurrency(amount){
   return "$" + amount.toFixed(2).replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}

function setCash(amount){
    $('#cash').text(toCurrency(amount));
}

function incrementCash(amount){
    CASH += amount;
    setCash(CASH);
}

function payrollPayable(){
    var $payrollBtn = $('#payrollBtn');
    if($payrollBtn.is(":visible")){
        (CASH >= $payrollBtn.val()) ? $payrollBtn.attr('disabled', false).addClass('btn-success') : $payrollBtn.attr('disabled', true).removeClass('btn-success');
    }
}

function detailCar(incrementAmount){
    TEMP_CLICK_HOLDER += incrementAmount;
    if((TEMP_CLICK_HOLDER >= CLICKS_TO_DETAIL_CAR) && (CURRENT_BASIC_SUPPLIES >= BASIC_SUPPLIES_PER_CAR)){
        CASH += CAR_COMPLETED_CASH_BONUS;
        setCash(CASH); //timing for updateBasicSupplies enable/disable
        CARS_ON_LOT--;
        updateBasicSupplies();
        TOTAL_CARS_COMPLETED++;
        $('.totalCarsCompletedValue').text(TOTAL_CARS_COMPLETED);
        setCarsOnLot();
        TEMP_CLICK_HOLDER = 0;
    }
}

function detailCutAndBuffCar(incrementAmount){
    TEMP_CLICK_HOLDER_CAB += incrementAmount;
    if((TEMP_CLICK_HOLDER_CAB >= CLICKS_TO_DETAIL_CAR_CAB) && (CURRENT_SUPPLIES_CAB >= SUPPLIES_PER_CAR_CAB)){
        CASH += CAR_COMPLETED_CASH_BONUS_CAB;
        setCash(CASH);
        CARS_ON_LOT_CAB--;
        updateCutAndBuffSupplies();
        TOTAL_CARS_COMPLETED++;
        $('.totalCarsCompletedValue').text(TOTAL_CARS_COMPLETED);
        setCutAndBuffCarsOnLot();
        TEMP_CLICK_HOLDER_CAB = 0;
    }
}

function updateBasicSupplies(){
    CURRENT_BASIC_SUPPLIES -= BASIC_SUPPLIES_PER_CAR;
    var costToRefill = (BASIC_SUPPLIES_CAPACITY - CURRENT_BASIC_SUPPLIES) * BASIC_SUPPLIES_COST_PER_UNIT;
    var $progressBar = $('#basicSuppliesProgressBar');
    var $buyBtn = $('#buyBasicSupplies');
    if(CURRENT_BASIC_SUPPLIES <= BASIC_SUPPLIES_PER_CAR && BASIC_SUPPLY_AUTOMATION_UNLOCKED){
        fillSupplies(costToRefill, $progressBar, $buyBtn);
        CURRENT_BASIC_SUPPLIES = BASIC_SUPPLIES_CAPACITY;
        costToRefill = 0;
    }
    var percentageOfFull = (CURRENT_BASIC_SUPPLIES / BASIC_SUPPLIES_CAPACITY) * 100;
    $progressBar.css('width', percentageOfFull+'%').attr('aria-valuenow', CURRENT_BASIC_SUPPLIES);
    $buyBtn.text('Fill - $' + costToRefill);
    (CASH >= costToRefill) ? $buyBtn.attr('disabled', false).addClass('btn-success') : $buyBtn.attr('disabled', true).removeClass('btn-success');
}

function updateCutAndBuffSupplies(){
    CURRENT_SUPPLIES_CAB -= SUPPLIES_PER_CAR_CAB;
    var costToRefill = (SUPPLIES_CAPACITY_CAB - CURRENT_SUPPLIES_CAB) * SUPPLIES_COST_PER_UNIT_CAB;
    var $progressBar = $('#cutAndBuffSuppliesProgressBar');
    var $buyBtn = $('#buyCutAndBuffSupplies');
    if(CURRENT_SUPPLIES_CAB <= SUPPLIES_PER_CAR_CAB && CUT_AND_BUFF_SUPPLY_AUTOMATION_UNLOCKED){
        fillSupplies(costToRefill, $progressBar, $buyBtn);
        CURRENT_SUPPLIES_CAB = SUPPLIES_CAPACITY_CAB;
        costToRefill = 0;
    }
    var percentageOfFull = (CURRENT_SUPPLIES_CAB / SUPPLIES_CAPACITY_CAB) * 100;
    $progressBar.css('width', percentageOfFull+'%').attr('aria-valuenow', CURRENT_SUPPLIES_CAB);
    $buyBtn.text('Fill - $' + costToRefill);
    (CASH >= costToRefill) ? $buyBtn.attr('disabled', false).addClass('btn-success') : $buyBtn.attr('disabled', true).removeClass('btn-success');
}

function fillSupplies(costToRefill, $progressBar, $buyBtn){
    incrementCash(-(costToRefill));
    $progressBar.css('width', '100%').attr('aria-valuenow', 100);
    $buyBtn.text('Fill - $0').attr('disabled', true).removeClass('btn-success');
}

function workerUnlockConditionsMet(){
    if(GENERAL_MANAGER_HIRED && workersUpgradable()){
        if(MISSED_PAYROLLS < MISSED_PAYROLLS_THRESHOLD){
            unlockHireWorkersButton(true);
        }
    }else{
        unlockHireWorkersButton(false);
    }
}

function unlockHireWorkersButton(enableButton){
    if(enableButton && GENERAL_MANAGER_HIRED){
        $('#hireWorkerBtn').attr('disabled', false).addClass('btn-success');
        $('#hireWorkerCutAndBuffBtn').attr('disabled', false).addClass('btn-success');
    }else{
        $('#hireWorkerBtn').attr('disabled', true).removeClass('btn-success');
        $('#hireWorkerCutAndBuffBtn').attr('disabled', true).removeClass('btn-success');
    }
}

function unlockLotCapacityButton(enableButton){
    enableButton ? $('#increaseLotCapacityBtn').attr('disabled',false).addClass('btn-success') : $('#increaseLotCapacityBtn').attr('disabled',true).removeClass('btn-success');
}

function unlockCutAndBuffLotCapacityButton(enableButton){
    enableButton ? $('#increaseLotCapacityCutAndBuffBtn').attr('disabled',false).addClass('btn-success') : $('#increaseLotCapacityCutAndBuffBtn').attr('disabled',true).removeClass('btn-success');
}

function notificationsCheck(){
    if($('.notificationsContent').children(':visible').length == 0) {
        $('#notificationLbl').show();
    }
}

function updateNotifications($element){
    if($element !== "undefined"){
        $.each($('.notificationsContent').children().not($element), function(){
            $(this).hide();
        });
        //todo. the label currently stays indefinitely instead of showing and hiding. using blink() introduces a bug in notificationsCheck() because the label is temporarily not visible
        if($element.is("label")){ //only temporarily display labels
            $element.fadeIn(500);
//            $element.hide();
        }else{
            $element.fadeIn(500);
        }
    }
    notificationsCheck();
}

function formatCard($card){
    $card.replaceWith('<span class="oi oi-check" style="color:#28a745"></span><br >');
}

function showContent($content){
    $('#contentDiv').children(":visible").hide();
    $content.show(); //content must be within #contentDiv to display
}

$(window).scroll(function(){
    var $summaryContainer = $('.summaryContainer');
    if($(this).scrollTop() > $summaryContainer.height()){
        $summaryContainer.addClass('sticky');
        $('.summaryContainerPlaceholder').css('display','block');
    }else{
        $summaryContainer.removeClass('sticky');
        $('.summaryContainerPlaceholder').css('display','none');
    }
});