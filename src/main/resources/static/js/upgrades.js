function upgradesUnlocked(){
    if($('.buyBtn').is(":visible")){
        $('.buyBtn').each(function(i){
            var $this = $(this);
            if(CASH >= $this.val()){
                $this.attr('disabled', false);
            }else{
                $this.attr('disabled', true);
            }
        });
    }
}

function upgradeBought($card){
    var category = $card.closest("div").prop("id");
    incrementCash(-($card.val()));
    formatCard($card);
    updateFooterStats(category);
}

function updateFooterStats(category){
    switch(category) {
      case "productionUpgradeCards":
        updateProductionStats();
        break;
    case "workerUpgradeCards":
        updateWorkerStats();
        break;
      default:
    }
}

function updateProductionStats(){
    $('.carFrequencyLbl').text("x" + CAR_FREQUENCY_STAT);
    $('.basicSupplyCapacityLbl').text("+" + SUPPLY_CAPACITY_STAT);
    $('.lotCapacityLbl').text("+" + LOT_CAPACITY_STAT);
    $('.carBonusLbl').text("+$" + CAR_PRICE_STAT);
}

function updateWorkerStats(){
    $('.workerSpeedLbl').text("+" + WORKER_SPEED_STAT + "%");
    $('.workerEfficiencyLbl').text("+" + WORKER_EFFICIENCY_STAT + "%");
}

function nextLevelUnlocked(){
    if(TOTAL_CARS_COMPLETED >= UNLOCK_NEXT_LEVEL_CARS){
        CURRENT_LEVEL += 1;
        $('.currentLevelValue').text(CURRENT_LEVEL);
        UNLOCK_NEXT_LEVEL_CARS *= UNLOCK_NEXT_LEVEL_MULTIPLIER;
    }
    upgradeCategoriesUnlocked();
}

function upgradeCategoriesUnlocked(){
    if(!PRODUCTIVITY_UPGRADE_UNLOCKED){
        if(CURRENT_LEVEL >= UNLOCK_PRODUCTIVITY_UPGRADES_LEVEL){
            PRODUCTIVITY_UPGRADE_UNLOCKED = true;
            $('#productionCategoryCard').removeClass('locked');
        }
    }
    if(!WORKER_UPGRADE_UNLOCKED){
        if(CURRENT_LEVEL >= UNLOCK_WORKER_UPGRADES_LEVEL){
            WORKER_UPGRADE_UNLOCKED = true;
            $('.workerCategoryCard').removeClass('locked');
        }
    }
    if(!MANAGER_UPGRADE_UNLOCKED){
        if(anyManagerHired()){
            MANAGER_UPGRADE_UNLOCKED = true;
            $('.managerCategoryCard').removeClass('locked');
        }
    }
    if(!SPECIALITY_UPGRADE_UNLOCKED){
        if(OPERATIONS_MANAGER_HIRED){
            SPECIALITY_UPGRADE_UNLOCKED = true;
            $('.specialityCategoryCard').removeClass('locked');
        }
    }
}

function anyManagerHired(){
    return (ACCOUNTANT_HIRED || GENERAL_MANAGER_HIRED || OPERATIONS_MANAGER_HIRED);
}

//todo just give these common classes instead
$('#buyLevel1Advertising, #buyLevel2Advertising, #buyLevel3Advertising').click(function(){
    CAR_SPAWN_TIME /= 2;
    CAR_FREQUENCY_STAT += 2;
    upgradeBought($(this));
});

$('#buyLevel1WorkerProductivity, #buyLevel2WorkerProductivity').click(function(){
    WORKER_DETAIL_CAR_INCREMENT *= 1.20;
    WORKER_SPEED_STAT += 20;
    upgradeBought($(this));
});

$('#buyLevel1LotCapacity').click(function(){
    LOT_CAPACITY_THRESHOLD += 2;
    LOT_CAPACITY_STAT += 2;
    upgradeBought($(this));
});

$('#buyCompletedCarBonus').click(function(){
    CAR_COMPLETED_CASH_BONUS += 50;
    CAR_PRICE_STAT += 50;
    upgradeBought($(this));
});

$('#buyInflation').click(function(){
    CAR_COMPLETED_CASH_BONUS += 15;
    CAR_PRICE_STAT += 15;
    upgradeBought($(this));
});

$('#buySupplyCapacity').click(function(){
    BASIC_SUPPLIES_CAPACITY += 10;
    SUPPLY_CAPACITY_STAT += 10;
    $('#basicSuppliesProgressBar').attr('aria-valuemax',BASIC_SUPPLIES_CAPACITY);
    upgradeBought($(this));
});

$('#buySupplyCapacity2').click(function(){
    BASIC_SUPPLIES_CAPACITY += 50;
    SUPPLY_CAPACITY_STAT += 50;
    $('#basicSuppliesProgressBar').attr('aria-valuemax',BASIC_SUPPLIES_CAPACITY);
    upgradeBought($(this));
});

$('#buyWorkersMoreEfficient').click(function(){
    BASIC_SUPPLIES_PER_CAR -= 2;
    WORKER_EFFICIENCY_STAT += 20;
    upgradeBought($(this));
});

$('#buyBasicSupplyAutomation').click(function(){
    BASIC_SUPPLY_AUTOMATION_UNLOCKED = true;
    upgradeBought($(this));
    $('#buyBasicSupplies').click();
});

$('#buyCutAndBuffSupplyAutomation').click(function(){
    CUT_AND_BUFF_SUPPLY_AUTOMATION_UNLOCKED = true;
    upgradeBought($(this));
    $('#buyCutAndBuffSupplies').click();
});

$('#buyCookTheBooks').click(function(){
     CAR_COMPLETED_CASH_BONUS += 100;
     CAR_PRICE_STAT += 100;
     upgradeBought($(this));
});

$('#buyCutandBuffWorkerUnlock').click(function(){
    $(".cutAndBuffLocked").removeClass("cutAndBuffLocked");
    $('.cutAndBuffLockedListItem .lockIcon').remove();
    $('#managerUpgradeCards a').removeClass('cutAndBuffLockedListItem');
    $('.cutAndBuffLockedListItem .buyBtn').show();
    CUT_AND_BUFF_UNLOCKED = true;
    upgradeBought($(this));
});