$('.footer-button-group button').click(function(){
    var $this = $(this);
    var $thisContent = $('#' + $this.val());
    formatSelectedButton($this);
    showContent($thisContent);
});

function formatSelectedButton($btn){
    $btn.find('.selectedFooterButton').show();
    $btn.find('.oi').addClass('selectedFooterButton');
    var remainingButtons = $('.footer-button-group button').not($btn);
    $.each(remainingButtons, function(){
        var $this = $(this);
        $this.find('.oi').removeClass('selectedFooterButton');
        $this.find('.selectedFooterButton').hide();
    });
}
