$(document).ready(function() {

//not sure why these dont load correctly when in the jsIncludes.html
    $.when(
        $.getScript("../../js/workers.js"),
        $.getScript("../../js/managers.js"),
        $.getScript("../../js/upgradeCategories.js"),
        $.getScript("../../js/upgrades.js"),
        $.getScript("../../js/carLot.js"),
        $.getScript("../../js/debug.js"),
        $.getScript("../../js/footerButtons.js"),
        $.Deferred(function(deferred){
            $(deferred.resolve);
        })
    ).done(function(){
        initialize();
        start();
    });

    function start() {
        window.setInterval(function(){
            spawnCars();
            spawnCutAndBuffCars();
            ((CARS_ON_LOT > 0) && (CURRENT_BASIC_SUPPLIES > BASIC_SUPPLIES_PER_CAR)) ? $('#detailBtn').attr('disabled',false) : $('#detailBtn').attr('disabled',true);
            ((CARS_ON_LOT_CAB > 0) && (CURRENT_SUPPLIES_CAB > SUPPLIES_PER_CAR_CAB)) ? $('#detailCutAndBuffBtn').attr('disabled',false) : $('#detailCutAndBuffBtn').attr('disabled',true);
            workerUnlockConditionsMet();
            (lotCapacityUpgradable($('#increaseLotCapacityBtn')) && !lotThresholdMet()) ? unlockLotCapacityButton(true) : unlockLotCapacityButton(false);
            (lotCapacityUpgradable($('#increaseLotCapacityCutAndBuffBtn')) && !cutAndBuffLotThresholdMet()) ? unlockCutAndBuffLotCapacityButton(true) : unlockCutAndBuffLotCapacityButton(false);
            if(WORKERS > 0){
                if(CARS_ON_LOT > 0){
                    detailCar(WORKER_DETAIL_CAR_INCREMENT * WORKERS);
                }
                TEMP_WORKER_ELAPSED_TIME += 100;
            }
            if(WORKERS_CAB > 0){
                if(CARS_ON_LOT_CAB > 0){
                    detailCutAndBuffCar(WORKER_DETAIL_INCREMENT_CAB * WORKERS_CAB);
                }
                TEMP_WORKER_ELAPSED_TIME_CAB += 100;
            }
            setCash(CASH);
            payrollPayable();
            payrollDue();
            upgradesUnlocked();
            nextLevelUnlocked();
//            if($('.metrics').is(":visible")){
//                updateMetrics();
//            }
            setDebugFields();
            ELAPSED_TIME += 100;
        }, 100)
    }

    $('#detailBtn').on('click', function(){
        detailCar(USER_DETAIL_CAR_INCREMENT);
    });

    $('#detailCutAndBuffBtn').on('click', function(){
        detailCutAndBuffCar(USER_DETAIL_CUT_AND_BUFF_CAR_INCREMENT);
    });

    $('#payrollBtn').click(function(){
        $('#workers').css("color","#939393");
        $('#workersCutAndBuff').css("color","#939393");
        incrementCash(-($(this).val()));
        $(this).val(0).hide();
        notificationsCheck();
        MISSED_PAYROLLS = 0;
        unlockHireWorkersButton(true);
    });

});