function managerHired($buyBtn){
    incrementCash(-($buyBtn.val()));
    formatCard($buyBtn);
}

$('#buyAccountant').click(function(){
    ACCOUNTANT_HIRED = true;
    $('.accountantLockedListItem .lockIcon').remove();
    $('#managerUpgradeCards a').removeClass('accountantLockedListItem');
    $('.upgradeManagerDetailDiv .tab-pane').removeClass('accountantLockedCard');
    $('.accountantLockedListItem .buyBtn').show();
    managerHired($(this));
});

$('#buyGeneralManager').click(function(){
    GENERAL_MANAGER_HIRED = true;
    $('.generalManagerLockedListItem .lockIcon').remove();
    $('.generalManagerLockedListItem .buyBtn').show();
    $('#managerUpgradeCards a').removeClass('generalManagerLockedListItem');
    $('.upgradeManagerDetailDiv .tab-pane').removeClass('generalManagerLockedCard');
    managerHired($(this));
});

$('#buyOperationsManager').click(function(){
    OPERATIONS_MANAGER_HIRED = true;
    managerHired($(this));
});

