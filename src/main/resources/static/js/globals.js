/***********************
    GAME GLOBALS
************************/
var ELAPSED_TIME = 0;



/***********************
    USER GLOBALS
************************/
var USER_DETAIL_CAR_INCREMENT = 1.5;
var USER_DETAIL_CUT_AND_BUFF_CAR_INCREMENT = 1.5;



/***********************
    CASH GLOBALS
************************/
var CASH = 0;



/***********************
    MANAGER GLOBALS
************************/
ACCOUNTANT_HIRED = false;
GENERAL_MANAGER_HIRED = false;
OPERATIONS_MANAGER_HIRED = false;



/***********************
    WORKER GLOBALS
************************/
var WORKERS = 0;
var WORKER_UPGRADE_PRICE = 200;
var WORKER_PAY = 100;
var WORKER_DETAIL_CAR_INCREMENT = .15;

    /*   WORKER PAYROLL    */
    var TEMP_WORKER_ELAPSED_TIME = 0;
    var WORKER_PAYROLL_DUE = 60000;
    var MISSED_PAYROLLS = 0;
    var MISSED_PAYROLLS_THRESHOLD = 3;

    /*   CUT & BUFF    */
    var WORKERS_CAB = 0;
    var WORKER_PAY_CAB = 400;
    var WORKER_DETAIL_INCREMENT_CAB = .15;
    var TEMP_WORKER_ELAPSED_TIME_CAB = 0;
    var WORKER_UPGRADE_PRICE_CAB = 400;



/***********************
    LOT GLOBALS
************************/
var CURRENT_LOT_CAPACITY = 0;
var LOT_CAPACITY_THRESHOLD = 4;
var LOT_UPGRADE_PRICE = 75;

    /*   CUT & BUFF   */
    var CURRENT_LOT_CAPACITY_CAB = 0;
    var LOT_CAPACITY_THRESHOLD_CAB = 2;
    var LOT_UPGRADE_PRICE_CAB = 300;



/***********************
    CAR GLOBALS
************************/
var TEMP_CLICK_HOLDER = 0;
var CLICKS_TO_DETAIL_CAR = 10;
var CARS_PER_SEC = 0;
var CAR_SPAWN_TIME = 2500;
var TEMP_ELAPSED_TIME = 0; //todo use ELAPSED_TIME instead
var CARS_ON_LOT = 1;
var CAR_COMPLETED_CASH_BONUS = 50;
var TOTAL_CARS_COMPLETED = 0;

    /*   CUT & BUFF   */
    var TEMP_ELAPSED_TIME_CAB = 0;
    var TEMP_CLICK_HOLDER_CAB = 0;
    var CLICKS_TO_DETAIL_CAR_CAB = 20;
    var CAR_SPAWN_TIME_CAB = 10000;
    var CARS_ON_LOT_CAB = 1;
    var CAR_COMPLETED_CASH_BONUS_CAB = 400;



/***********************
    LEVEL GLOBALS
************************/
var CURRENT_LEVEL = 1;
var UNLOCK_NEXT_LEVEL_CARS = 5;
var UNLOCK_NEXT_LEVEL_MULTIPLIER = 4;



/***********************
    UPGRADE GLOBALS
************************/
var UNLOCK_PRODUCTIVITY_UPGRADES_LEVEL = 3;
var UNLOCK_WORKER_UPGRADES_LEVEL = 5;
var CUT_AND_BUFF_UNLOCKED = false;

    /*    MANAGER UNLOCKS     */
    var PRODUCTIVITY_UPGRADE_UNLOCKED = false;
    var WORKER_UPGRADE_UNLOCKED = false;
    var MANAGER_UPGRADE_UNLOCKED = false;
    var SPECIALITY_UPGRADE_UNLOCKED = false;

    /*    PRODUCTION STATS    */
    var CAR_FREQUENCY_STAT = 0;
    var SUPPLY_CAPACITY_STAT = 0;
    var LOT_CAPACITY_STAT = 0;
    var CAR_PRICE_STAT = 0;

     /*    WORKER STATS       */
    var WORKER_SPEED_STAT = 0;
    var WORKER_EFFICIENCY_STAT = 0;



/***********************
    SUPPLY GLOBALS
************************/
var BASIC_SUPPLIES_PER_CAR = 10;
var BASIC_SUPPLIES_CAPACITY = 100;
var BASIC_SUPPLIES_COST_PER_UNIT = 1;
var CURRENT_BASIC_SUPPLIES = 100;
var BASIC_SUPPLY_AUTOMATION_UNLOCKED = false;

    /*   CUT & BUFF   */
    var SUPPLIES_PER_CAR_CAB = 12;
    var SUPPLIES_CAPACITY_CAB = 100;
    var SUPPLIES_COST_PER_UNIT_CAB = 6;
    var CURRENT_SUPPLIES_CAB = 100;
    var CUT_AND_BUFF_SUPPLY_AUTOMATION_UNLOCKED = false;