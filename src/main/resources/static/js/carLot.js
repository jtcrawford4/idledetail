function setLotCapacity(amount){
    CURRENT_LOT_CAPACITY += amount;
    setCarsOnLot();
}

function setLotCapacityCutAndBuff(amount){
    CURRENT_LOT_CAPACITY_CAB += amount;
    setCutAndBuffCarsOnLot();
}

function setCarsOnLot(){
    $('#carsOnLot').text(CARS_ON_LOT + " / " + CURRENT_LOT_CAPACITY);
}

function setCutAndBuffCarsOnLot(){
    $('#carsOnLotCutAndBuff').text(CARS_ON_LOT_CAB + " / " + CURRENT_LOT_CAPACITY_CAB);
}

function lotCapacityUpgradable($lotCapacityBtn){
    return (CASH >= parseInt($lotCapacityBtn.val()))
}

function setLotCapacityCost(amount){
    $('#increaseLotCapacityBtn').val(amount);
    LOT_UPGRADE_PRICE = amount;
}

function setCutAndBuffLotCapacityCost(amount){
    $('#increaseLotCapacityCutAndBuffBtn').val(amount);
    LOT_UPGRADE_PRICE_CAB = amount;
}

function lotHasCapacity(){
    return CURRENT_LOT_CAPACITY > CARS_ON_LOT;
}

function cutAndBufflotHasCapacity(){
    return CURRENT_LOT_CAPACITY_CAB > CARS_ON_LOT_CAB;
}

function lotThresholdMet(){
    return CURRENT_LOT_CAPACITY == LOT_CAPACITY_THRESHOLD;
}

function cutAndBuffLotThresholdMet(){
    return CURRENT_LOT_CAPACITY_CAB == LOT_CAPACITY_THRESHOLD_CAB;
}

function spawnCars(){
    TEMP_ELAPSED_TIME += 100;
    if(carsCanSpawn() && lotHasCapacity()){
        CARS_ON_LOT += 1;
        setCarsOnLot();
        TEMP_ELAPSED_TIME = 0;
    }
}

function spawnCutAndBuffCars(){
    TEMP_ELAPSED_TIME_CAB += 100;
    if(cutAndBuffCarsCanSpawn() && cutAndBufflotHasCapacity()){
        CARS_ON_LOT_CAB += 1;
        setCutAndBuffCarsOnLot();
        TEMP_ELAPSED_TIME_CAB = 0;
    }
}

function carsCanSpawn(){
    return TEMP_ELAPSED_TIME >= CAR_SPAWN_TIME;
}

function cutAndBuffCarsCanSpawn(){
    return TEMP_ELAPSED_TIME_CAB >= CAR_SPAWN_TIME_CAB;
}

$('#increaseLotCapacityBtn').on('click', function(){
    setLotCapacity(1);
    incrementCash(-(LOT_UPGRADE_PRICE))
    setLotCapacityCost(LOT_UPGRADE_PRICE * 3.5); //todo value?
});

$('#increaseLotCapacityCutAndBuffBtn').on('click', function(){
    setLotCapacityCutAndBuff(1);
    incrementCash(-(LOT_UPGRADE_PRICE_CAB))
    setCutAndBuffLotCapacityCost(LOT_UPGRADE_PRICE_CAB * 3.5);
});