function setDebugFields(){
    $('.debugCurrentLevel').text(CURRENT_LEVEL);
    $('.debugWorkerUpgradePrice').text(toCurrency(WORKER_UPGRADE_PRICE));
    $('.debugUnlockNextLevelCost').text(UNLOCK_NEXT_LEVEL_CARS);
//    $('.debugWorkerSpeed').text(parseFloat((CLICKS_TO_DETAIL_CAR / (WORKER_DETAIL_CAR_INCREMENT / WORKERS * 100)).toFixed(2)));
//    $('.debugUnlockNextLevelCost').text(toCurrency(UNLOCK_NEXT_LEVEL_COST));
    $('.debugLotUpgradePrice').text(toCurrency(LOT_UPGRADE_PRICE));
    $('.debugCarSpawnTime').text(CAR_SPAWN_TIME + " ms");
    $('.debugLotCapacityThreshold').text(LOT_CAPACITY_THRESHOLD);
}

//todo make hotkey to display button. show div in footer
$('#toggleDebug').on('click', function(){
    $('#debug').toggle();
});

$('.addCash').click(function(){
    incrementCash(1000000);
});

$('.increaseLevel').click(function(){
    CURRENT_LEVEL = 20;
});