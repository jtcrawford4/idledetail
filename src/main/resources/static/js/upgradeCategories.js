function isLocked($btn){
    return $btn.hasClass('locked');
}

function lockedManagerCardFormatting($element){
var correspondingId = $element.attr('aria-controls');
var $card = $('.upgradeManagerDetailDiv').find('#' + correspondingId);
    if($element.hasClass('generalManagerLockedListItem') || $element.hasClass('accountantLockedListItem') || $element.hasClass('cutAndBuffLockedListItem')){
        $card.find('.lockedCard').show();
        $card.find('.unlockedCard').hide();
    }else{
        $card.find('.lockedCard').hide();
        $card.find('.unlockedCard').show();
    }
}

//todo these can all be utilizing one function...
$('#productionCategoryCard').click(function(){
    if(!isLocked($(this))){
        showContent($('#contentDiv').find($('#productionUpgrades')));
    }
});

$('.workerCategoryCard').click(function(){
     if(!isLocked($(this))){
        showContent($('#contentDiv').find($('#workerUpgrades')));

    }
});

$('.managerCategoryCard').click(function(){
     if(!isLocked($(this))){
        showContent($('#contentDiv').find($('#managerUpgrades')));
        $('#list-basic-supply').click();
    }
});

$('.specialityCategoryCard').click(function(){
     if(!isLocked($(this))){
        showContent($('#contentDiv').find($('#specialityUpgrades')));
    }
});

$('#managerUpgradeCards .upgrade-group-item').click(function(){
    lockedManagerCardFormatting($(this));
});