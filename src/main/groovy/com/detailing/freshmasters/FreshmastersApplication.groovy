package com.detailing.freshmasters

import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.SpringBootApplication

@SpringBootApplication
class FreshmastersApplication {

	static void main(String[] args) {
		SpringApplication.run FreshmastersApplication, args
	}
}
