package com.detailing.freshmasters.controller

import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.RequestMapping

@Controller
class HomeController {

    @RequestMapping("")
    String home(){
        "home"
    }

}
